# Exemple d'Utilisation de mkdocs-dsfr

Ce dépôt sert d'exemple pour le thème [mkdocs-dsfr](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/mkdocs-dsfr), un thème mkdocs conforme aux standards de l'État français pour la création de sites Web.

- Voir cet exemple sur [la page Gitlab](https://pub.gitlab-pages.din.developpement-durable.gouv.fr/numeco/mkdocs-dsfr-exemple)

## Prérequis

- [Python 3.x](https://www.python.org/downloads/)
- [pipenv](https://pipenv.pypa.io/en/latest/)

## Installation

1. Clonez ce dépôt sur votre machine locale.
2. Ouvrez un terminal et naviguez vers le dossier du projet.
3. Exécutez `pipenv install` pour installer les dépendances du projet.

## Utilisation

### Environnement de Développement

1. Entrez dans l'environnement virtuel avec `pipenv shell`.
2. Lancez le serveur de développement avec `mkdocs serve`.
3. Ouvrez votre navigateur Web et accédez à `http://127.0.0.1:8000`.

### Construction du Site

Pour construire le site Web statique, utilisez la commande suivante :

```bash
mkdocs build
```

Le site sera généré dans le dossier `site`.

## Contributeurs

Ce projet est une initiative du gouvernement français pour promouvoir les standards de développement Web en France.

## Licence

MIT

## Ressources Utiles

- [Site officiel de DSFR](https://www.systeme-de-design.gouv.fr/)
- [Documentation mkdocs](https://www.mkdocs.org/)

---
