# Les outils du Media Centre dans Lycée Connecté

Bienvenue dans ce guide d'astuces .......
Outils jeux en H5P.

## Table des Matières

1. [Introduction](#introduction)
2. [Conseils Généraux](#conseils-généraux)
3. [Trucs Pratiques](#trucs-pratiques)
4. [Tableau des Méthodes Utiles](#tableau-des-méthodes-utiles)
5. [Références](#références)

---

## Introduction

<iframe
  id="inlineFrameExample"
  title="Inline Frame Example"
  width=100%
  height=800px
  scrolling="no"
  frameborder="0" 
  src="../LC.html">
</iframe>

---

## Conseils Généraux



---

## Trucs Pratiques

### Décomposition (Destructuring)

