
# Lorem ipsum dolor sit amet, consectetur adipiscing elit.

## Table des Matières

1. [Lorem Ipsum](#lorem-ipsum)
2. [Dolor Sit Amet](#dolor-sit-amet)
3. [Consectetur Adipiscing](#consectetur-adipiscing)

---

## Lorem Ipsum

**Lorem ipsum** dolor sit amet, consectetur adipiscing elit. Vestibulum auctor purus sit amet ultricies aliquam.

```javascript
// Exemple de code
console.log("Lorem ipsum!");
```

---

## Dolor Sit Amet

Vivamus accumsan, libero non dictum lacinia, enim elit aliquet dolor, id faucibus dolor ipsum a libero.

| Exemple | Description | Note |
|---------|-------------|------|
| Lorem   | Ipsum       | 5/5  |
| Dolor   | Sit Amet    | 4/5  |
| Ipsum   | Sit Raket   | 2/5  |

---

## Consectetur Adipiscing

Ut sit amet urna porttitor, efficitur urna sit amet, cursus ligula. Duis sit amet laoreet turpis.

```javascript
const example = "Dolor Sit Amet";
```

---

## Références

- [Lorem Ipsum](https://www.lipsum.com/)
- [Dolor Sit Amet](https://www.example.com/)

---
