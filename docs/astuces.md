# Astuces et Conseils pour JavaScript

Bienvenue dans ce guide d'astuces et de conseils pour JavaScript ! Vous découvrirez ici quelques techniques pour écrire
un code JavaScript plus efficace et propre.

## Table des Matières

1. [Introduction](#introduction)
2. [Conseils Généraux](#conseils-généraux)
3. [Trucs Pratiques](#trucs-pratiques)
4. [Tableau des Méthodes Utiles](#tableau-des-méthodes-utiles)
5. [Références](#références)

---

## Introduction

**JavaScript** est un langage flexible et puissant. Cependant, cette flexibilité peut aussi conduire à des erreurs ou à
un code difficile à maintenir si vous n'êtes pas vigilant.

---

## Conseils Généraux

- Utilisez `let` et `const` au lieu de `var` pour déclarer des variables.

```javascript
// Utilisation de let et const
let variable = "temporaire";
const CONSTANTE = "fixe";

// Fonction fléchée
const saluer = nom => "Bonjour, " + nom;
```

---

## Trucs Pratiques

### Décomposition (Destructuring)

Vous pouvez facilement extraire des propriétés d'un objet ou des éléments d'un tableau.

```javascript
const {nom, age} = {nom: 'Alice', age: 25};  // Décomposition d'objet
const [premier, deuxieme] = [1, 2];  // Décomposition de tableau
```

### Opérateur de Propagation (Spread Operator)

```javascript
const arr1 = [1, 2, 3];
const arr2 = [...arr1, 4, 5];  // [1, 2, 3, 4, 5]
```

---

## Tableau des Méthodes Utiles

| Méthode   | Exemple                            | Description                                             |
|-----------|------------------------------------|---------------------------------------------------------|
| `map`     | `arr.map(x => x * 2)`              | Transforme chaque élément du tableau.                   |
| `filter`  | `arr.filter(x => x > 0)`           | Filtrer les éléments du tableau.                        |
| `find`    | `arr.find(x => x > 0)`             | Trouver le premier élément qui satisfait une condition. |
| `reduce`  | `arr.reduce((a, b) => a + b, 0)`   | Réduire le tableau à une seule valeur.                  |
| `forEach` | `arr.forEach(x => console.log(x))` | Exécuter une fonction sur chaque élément.               |

---

## Références

- [JavaScript.info](https://javascript.info/)
- [MDN Web Docs - JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript)

---
